package richa.osahub.com.vaccikaranapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 28-07-2015.
 */
public class sOldVacciAdapter extends ArrayAdapter {
    LayoutInflater inflater;
    List<soldvacciprofile> list=new ArrayList<>();


    public sOldVacciAdapter(Activity activity, List<soldvacciprofile> objects) {
        super(activity, R.layout.activity_profile, objects);
        list=objects;
        inflater=activity.getWindow().getLayoutInflater();
    }
    @Override
    public View getView(int position,View view,ViewGroup parent)
    {
        View rootview=inflater.inflate(R.layout.activity_profile,parent,false);
        TextView oldvacci=(TextView)rootview.findViewById(R.id.sOldVacciName);
        TextView date=(TextView)rootview.findViewById(R.id.sdate);
        oldvacci.setText(list.get(position).getOldVacci());
        date.setText((CharSequence) list.get(position).getsDate());
        return rootview;

    }
}
