package richa.osahub.com.vaccikaranapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class SchedulerFragment extends Fragment {
    ArrayAdapter<VaccinationsPojo> vaccinationArrayAdapter;
    ListView listView;

    boolean IsPresent;

    public boolean isPresent() {
        return IsPresent;
    }

    public void setIsPresent(boolean isPresent) {
        IsPresent = isPresent;
    }

    SharedPreferences prefs=SchedulerFragment.this.getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE);
    SharedPreferences.Editor edit=prefs.edit();

    public SchedulerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_scheduler, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView);
        edit.putBoolean("IsPresent",IsPresent);
        DbHelper db = new DbHelper(getActivity().getApplicationContext());
        if(prefs.getBoolean("IsPresent",false)) {
            db.saveVaccinationList(db);
        }
        edit.apply();
        DbHelper db1 = new DbHelper(getActivity().getApplicationContext());

        Cursor dbCursor = db1.getVaccineForOneMonth(db1,0.1);
       /* Vaccinations vaccination = new Vaccinations("abc", "def","jkh jkh jhdgsdfghs sdfjsij dafh");
        Vaccinations vaccination1= new Vaccinations("abcd","hgdg","hgsdhug adhgdf dgdgjsa bdfhg");
        Vaccinations vaccination2= new Vaccinations("abcd","hgdg","hgsdhug adhgdf dgdgjsa bdfhg");*/

        List<VaccinationsPojo> vaccinations = new ArrayList<>();

        dbCursor.moveToFirst();
        while (!dbCursor.isLast()) {
            VaccinationsPojo vaccination = new VaccinationsPojo();
            vaccination.setVacciName(dbCursor.getString(0));
            vaccination.setDuration(dbCursor.getString(1));
            //vaccination.setPassword(dbCursor.getString(1));
            vaccinations.add(vaccination);
            dbCursor.moveToNext();
        }
        vaccinationArrayAdapter = new VaccinationListAdapter(getActivity(), vaccinations);
        listView.setAdapter(vaccinationArrayAdapter);
        return rootView;
    }


}
