package richa.osahub.com.vaccikaranapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class VacciKaran extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacci_karan);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        java.lang.Thread next = new java.lang.Thread(){
            public void run(){
                try{
                    sleep(2000);

                }catch(InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally {
                    Intent i = new Intent(getApplicationContext(),SigninActivity.class);
                    startActivity(i);
                }
            }
        };
        next.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}


