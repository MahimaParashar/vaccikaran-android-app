package richa.osahub.com.vaccikaranapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 7/28/2015.
 */
public class UpdateAdapter extends ArrayAdapter<UpdatePojo> {
    LayoutInflater inflater;
    List<UpdatePojo> updates = new ArrayList<>();

    public UpdateAdapter(Activity activity, List<UpdatePojo> objects) {
        super(activity,R.layout.update_list, objects);
        updates = objects;
        inflater=activity.getWindow().getLayoutInflater();
    }

    @Override
    public View getView(int position,View view,ViewGroup parent)
    {
        View rootview=inflater.inflate(R.layout.update_list,parent,false);
        TextView vacciName = (TextView)rootview.findViewById(R.id.vacciName);
        TextView time=(TextView)rootview.findViewById(R.id.time);



        vacciName.setText(updates.get(position).getVacciName());
        time.setText(updates.get(position).getTime());
        return rootview;
    }
}

