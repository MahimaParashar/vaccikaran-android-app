package richa.osahub.com.vaccikaranapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class SigninActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front);
        final EditText email;
        final EditText pass;
        final Button signin;
        final Button register;

        final SharedPreferences prefs = getSharedPreferences("Richie", MODE_PRIVATE);
        final SharedPreferences.Editor edit = prefs.edit();

        email = (EditText) findViewById(R.id.e_mail);
        pass = (EditText) findViewById(R.id.pas);

        signin = (Button) findViewById(R.id.signin);
        register = (Button) findViewById(R.id.register);

        if (prefs.contains("email")) {
            if (prefs.contains("password")) {
                email.setText(prefs.getString("email", "hello"));
                pass.setText(prefs.getString("password","hello"));
                if(!prefs.getString("email","").equals(""))
                {
                    if(!prefs.getString("password","").equals("")){
                        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(i);
                    }
                }

            }
        }

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), HomeActivity.class);
                //  edit.putString("user", email.getText().toString());
                //edit.putString("pass", pass.getText().toString());

                if (prefs.contains("email") && prefs.getString("email", "").equals(email.getText().toString())) {
                    if (prefs.contains("password") && prefs.getString("password", "").equals(pass.getText().toString())) {
                        edit.apply();
                        startActivity(in);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),"Invalid Password ... ",Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),"Invalid Email ..",Toast.LENGTH_LONG).show();
                }
            }

        });

        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Register.class);
                startActivity(intent);
            }
        });
    }
}

