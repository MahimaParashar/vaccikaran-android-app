package richa.osahub.com.vaccikaranapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Updates extends Fragment {
    ArrayAdapter<UpdatePojo> updateArrayAdapter;
    ListView listView;
    public Updates() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_notify, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView2);

        UpdatePojo update = new UpdatePojo("DPT","1 day");
        UpdatePojo update1 = new UpdatePojo("DPT","2 days");
        UpdatePojo update3 = new UpdatePojo("Hep B1","2 days");
        //UpdatePojo update = new UpdatePojo("abc","2 days");

        UpdatePojo update2 = new UpdatePojo("BCG - Injection","2 days");
        List<UpdatePojo> updates = new ArrayList<>();
        updates.add(update);
        updates.add(update1);
        updates.add(update3);
        updates.add(update2);


        updateArrayAdapter = new richa.osahub.com.vaccikaranapp.UpdateAdapter(getActivity(), updates);
        listView.setAdapter(updateArrayAdapter);

        return rootView;

    }

}
