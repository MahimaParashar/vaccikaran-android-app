package richa.osahub.com.vaccikaranapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;


public class NotificationSettings extends AppCompatActivity {
    CheckBox cb,cb1,cb2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_settings);
        cb = (CheckBox) findViewById(R.id.alarm);
        cb.setButtonDrawable(R.drawable.unchecked_checkbox);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    cb.setButtonDrawable(R.drawable.finalchecked);
                } else {
                    cb.setButtonDrawable(R.drawable.unchecked_checkbox);
                }
            }
        });
        cb1 = (CheckBox) findViewById(R.id.message);
        cb1.setButtonDrawable(R.drawable.finalchecked);
        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    cb1.setButtonDrawable(R.drawable.finalchecked);
                } else {
                    cb1.setButtonDrawable(R.drawable.unchecked_checkbox);
                }
            }
        });
        cb2 = (CheckBox) findViewById(R.id.email);
        cb2.setButtonDrawable(R.drawable.finalchecked);
        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    cb2.setButtonDrawable(R.drawable.finalchecked);
                } else {
                    cb2.setButtonDrawable(R.drawable.unchecked_checkbox);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notification_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
