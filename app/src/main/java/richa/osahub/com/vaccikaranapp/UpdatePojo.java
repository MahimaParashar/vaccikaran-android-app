package richa.osahub.com.vaccikaranapp;

import java.io.Serializable;

/**
 * Created by User on 7/28/2015.
 */
public class UpdatePojo implements Serializable {


    String vacciName;
    String time;

    public UpdatePojo(String vacciName, String time) {
        this.vacciName = vacciName;
        this.time = time;
    }
    public String getVacciName() {
        return vacciName;
    }

    public void setVacciName(String vacciName) {
        this.vacciName = vacciName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
