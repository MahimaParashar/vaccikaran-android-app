package richa.osahub.com.vaccikaranapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 9/3/2015.
 */
public class DbHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "vaccination_schedule";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public String CREATE_VACCINATION_TABLE_QUERY = "CREATE TABLE VACCINATION_LIST (AGE DOUBLE,VACCINATION VARCHAR)";

    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    SchedulerFragment schedulerFragmentObject=new SchedulerFragment();

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_VACCINATION_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void saveVaccinationList(DbHelper dbHelper) {
        schedulerFragmentObject.setIsPresent(true);
        schedulerFragmentObject.edit.putBoolean("IsPresent",schedulerFragmentObject.IsPresent);
       /* SQLiteDatabase sq = dbHelper.getWritableDatabase();

        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0','BCG')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0','OPV')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.1','OPV1')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.1','DPwT1')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.1','Hep B1')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.1','Hib 1*')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.2','OPV2')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.2','DPwT2')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.2','Hep B2')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.2','Hib 2*')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.3','OPV3')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.3','DPwT3')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.3','Hep B3')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.3','Hib 3*')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('0.9','Measles')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('1','1st booster of OPV/ DPwT')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('1','MMR*')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('5',' 2nd booster of DPwT')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('10','Tetanus Toxoid ')");
        sq.execSQL("INSERT INTO VACCINATION_LIST  VALUES('16','Tetanus Toxoid ')");
        */

        insertfunc(0, "BCG", dbHelper);
        insertfunc(0, "OPV", dbHelper);
        insertfunc(0.1, "OPV1", dbHelper);

        schedulerFragmentObject.edit.apply();
    }
    void insertfunc(double age, String vaccination,DbHelper dbHelper){
        SQLiteDatabase sq = dbHelper.getWritableDatabase();
        ContentValues cv= new ContentValues();
        cv.put("AGE",age);
        cv.put("VACCINATION",vaccination);
        sq.insert("VACCINATION_LIST",null,cv);

    }
    public Cursor getVaccineForOneMonth(DbHelper dbHelper,double age){
        SQLiteDatabase sq =dbHelper.getReadableDatabase();
        String columns[]={"VACCINATION","AGE"};
        String selectionargs[]={String.valueOf(age),"17"};
        return sq.query("VACCINATION_LIST",columns,"AGE BETWEEN ? AND ?",selectionargs,null,null,null);
    }


}
