package richa.osahub.com.vaccikaranapp;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Dell on 28-07-2015.
 */
public class soldvacciprofile implements Serializable {
    String oldVacci;
    Date sDate;
    public soldvacciprofile() {

    }

    public soldvacciprofile(String oldVacci, Date sDate) {
        this.oldVacci = oldVacci;
        this.sDate = sDate;
    }

    public String getOldVacci() {
        return oldVacci;
    }

    public void setOldVacci(String oldVacci) {
        this.oldVacci = oldVacci;
    }

    public Date getsDate() {
        return sDate;
    }

    public void setsDate(Date sDate) {
        this.sDate = sDate;
    }
}
