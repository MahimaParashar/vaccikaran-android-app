package richa.osahub.com.vaccikaranapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


public class SettingsFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        TextView profile,notification,settings,aboutUs,help,Signout;

        View rootview= inflater.inflate(R.layout.fragment_settings, container, false);
        profile=(TextView)rootview.findViewById(R.id.profile);
        notification=(TextView)rootview.findViewById(R.id.notification);
        settings=(TextView)rootview.findViewById(R.id.settings);
        aboutUs=(TextView)rootview.findViewById(R.id.about_us);
        help=(TextView)rootview.findViewById(R.id.help);
        Signout=(TextView)rootview.findViewById(R.id.signout);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(),Profile.class);
                startActivity(intent);

            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(getActivity().getApplicationContext(),NotificationSettings.class);
                startActivity(in);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte=new Intent(getActivity().getApplicationContext(),AboutUsActivity.class);
                startActivity(inte);
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten=new Intent(getActivity().getApplicationContext(),HelpActivity.class);
                startActivity(inten);
            }
        });
        Signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity().getApplicationContext(),SigninActivity.class);
                startActivity(intent);
            }
        });
        return rootview;
    }



}
